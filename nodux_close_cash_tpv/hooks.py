# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "nodux_close_cash_tpv"
app_title = "Cierre de Caja"
app_publisher = "NODUX"
app_description = "Reporte de Cierre de Caja"
app_icon = "octicon octicon-inbox"
app_color = "red"
app_email = "tatianaq@nodux.ec"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/nodux_close_cash_tpv/css/nodux_close_cash_tpv.css"
# app_include_js = "/assets/nodux_close_cash_tpv/js/nodux_close_cash_tpv.js"

# include js, css files in header of web template
# web_include_css = "/assets/nodux_close_cash_tpv/css/nodux_close_cash_tpv.css"
# web_include_js = "/assets/nodux_close_cash_tpv/js/nodux_close_cash_tpv.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "nodux_close_cash_tpv.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "nodux_close_cash_tpv.install.before_install"
# after_install = "nodux_close_cash_tpv.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "nodux_close_cash_tpv.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"nodux_close_cash_tpv.tasks.all"
# 	],
# 	"daily": [
# 		"nodux_close_cash_tpv.tasks.daily"
# 	],
# 	"hourly": [
# 		"nodux_close_cash_tpv.tasks.hourly"
# 	],
# 	"weekly": [
# 		"nodux_close_cash_tpv.tasks.weekly"
# 	]
# 	"monthly": [
# 		"nodux_close_cash_tpv.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "nodux_close_cash_tpv.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "nodux_close_cash_tpv.event.get_events"
# }


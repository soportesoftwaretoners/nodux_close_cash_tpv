from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Cierre de Caja"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "report",
					"name": "Cierre de Caja TPV",
					"doctype": "Sales Invoice",
					"is_query_report": True
				}

			]
		}
	]
